package com.example.administrator.websocketdemo;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.example.administrator.websocketdemo.service.SocketService;

public class MainActivity extends Activity {
    Intent service;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Intent service = new Intent(this, SocketService.class);
        startService(service);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopService(service);
    }
}
