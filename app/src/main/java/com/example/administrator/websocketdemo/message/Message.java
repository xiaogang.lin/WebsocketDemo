package com.example.administrator.websocketdemo.message;


import java.io.Serializable;

/**
 * 最基本的消息体
 * （具体要根据实际业务来定义这个报文的数据格式）
 */
public class Message implements Serializable {
    //推送id
    private String pushId;
    //消息类型
    private MessageType type;
    //具体的消息内容（有可能依然是json字符串）
    private String content;

    public Message() {

    }

    public String getPushId() {
        return pushId;
    }

    public void setPushId(String pushId) {
        this.pushId = pushId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public MessageType getType() {
        return type;
    }

    public void setType(MessageType type) {
        this.type = type;
    }

}

