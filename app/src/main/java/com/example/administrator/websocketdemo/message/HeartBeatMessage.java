package com.example.administrator.websocketdemo.message;


/**
 * 心跳报文
 */
public class HeartBeatMessage extends Message {
    //登录令牌
    private String token;

    public HeartBeatMessage(String token) {
        super();
        this.token = token;
        setType(MessageType.HEART_BEAT);
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}