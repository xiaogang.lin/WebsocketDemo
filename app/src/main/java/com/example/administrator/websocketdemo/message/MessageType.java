package com.example.administrator.websocketdemo.message;

/**
 * 报文类型
 * （具体根据实际业务来定义这个格式）
 */
public enum MessageType {

    HEART_BEAT(0),//心跳（双方通信）

    PUSH(1),//服务端推送报文过来

    PUSH_RESPONSE(2);//客户端反馈给服务端的报文，用来说明已收到服务端发来的消息

    private int type;

    public int gettype() {
        return type;
    }

    MessageType(int type) {
        this.type = type;
    }
}
