package com.example.administrator.websocketdemo.socket;

/**
 * 长连接自我检测的监听器，用来监听是否是本地或服务端断开长连接
 */
public interface SocketListener {
    void onDisconnected();
}
