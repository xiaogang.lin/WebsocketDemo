package com.example.administrator.websocketdemo.message;


/**
 * 推送给服务端的报文
 * （具体根据实际业务来定义这个报文的数据格式）
 */
public class PushResponseMessage extends Message {

    public PushResponseMessage(String content) {
        super();
        setType(MessageType.PUSH_RESPONSE);
    }

}